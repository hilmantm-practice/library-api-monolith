# How to run
## Prepare .env file
Make the env from .env.example codebase

## Run docker for postgres database
```docker compose -f docker-compose.local.yml --env-file .env up -d```

## Run migration
```migrate -database "postgres://<user>:<pass>@<host>:<port>/<db>" -path ./db/migrations/main up```

## Seeder for publisher
```go run ./db/seeder/main.go```

## Postman Collection
https://api.postman.com/collections/28317349-bb1a309e-3f95-4871-a167-819fd698f67d?access_key=PMAT-01H6MSZQFEBED1HET5CDN54P2X

## Postman Environment Example
```json
{
	"id": "9a6dbbaf-fb50-43e1-99b1-b9d90e073834",
	"name": "[local] library-api environment",
	"values": [
		{
			"key": "host",
			"value": "localhost:8000",
			"type": "default",
			"enabled": true
		},
		{
			"key": "version",
			"value": "v1",
			"type": "default",
			"enabled": true
		},
		{
			"key": "current_author_id",
			"value": "13eca396-fd94-4bb9-b94c-1d59ce96fa40",
			"type": "default",
			"enabled": true
		},
		{
			"key": "current_token",
			"value": "",
			"type": "any",
			"enabled": true
		}
	],
	"_postman_variable_scope": "environment",
	"_postman_exported_at": "2023-07-31T02:24:58.339Z",
	"_postman_exported_using": "Postman/10.16.3"
}
```