package app

import (
	"github.com/gin-gonic/gin"
	"library-api/config"
	"library-api/middleware"
	"library-api/modules/auth/v1/handler"
	"library-api/modules/auth/v1/service"
	handler2 "library-api/modules/library/v1/handler"
	service2 "library-api/modules/library/v1/service"
)

func SetApiRoute(route *gin.Engine) *gin.RouterGroup {
	return route.Group("/api")
}

func AuthHTTPHandler(router *gin.Engine, authUseCase service.AuthUseCase) {
	authHandler := handler.NewAuthHandler(authUseCase)
	apiRoute := SetApiRoute(router)
	v1 := apiRoute.Group("/v1")
	{
		v1.POST("/login", authHandler.Login)
	}
}

func AuthorHTTPHandler(
	config config.Config,
	router *gin.Engine,
	authorCreatorUseCase service2.AuthorCreatorUseCase,
	authorDeletorUseCase service2.AuthorDeleterUseCase,
	authorUpdaterUseCase service2.AuthorUpdaterUseCase) {
	apiRoute := SetApiRoute(router)

	authorCreatorHandler := handler2.NewAuthorCreatorHandler(authorCreatorUseCase)
	authorDeleterHandler := handler2.NewAuthorDeleterHandler(authorDeletorUseCase)
	authorUpdaterHandler := handler2.NewAuthorUpdaterHandler(authorUpdaterUseCase)

	v1 := apiRoute.Group("/v1")
	{
		v1.POST("/register", authorCreatorHandler.Register)

		author := v1.Group("/author").Use(middleware.Auth(config))
		{
			author.DELETE("/:id", authorDeleterHandler.DeleteAuthor)
			author.PUT("/:id", authorUpdaterHandler.UpdateAuthor)
		}
	}

}

func PublisherHTTPHandler(
	config config.Config,
	router *gin.Engine,
	publisherCreatorUseCase service2.PublisherCreatorUseCase,
	publisherDeleterUseCase service2.PublisherDeleterUseCase,
	publisherUpdaterUseCase service2.PublisherUpdaterUseCase,
	publisherFinderUseCase service2.PublisherFinderUseCase) {

	apiRoute := SetApiRoute(router)

	publisherCreatorHandler := handler2.NewPublisherCreatorHandler(publisherCreatorUseCase)
	publisherDeleterHandler := handler2.NewPublisherDeleterHandler(publisherDeleterUseCase)
	publisherUpdaterHandler := handler2.NewPublisherUpdaterHandler(publisherUpdaterUseCase)
	publisherFinderHandler := handler2.NewPublisherFinderHandler(publisherFinderUseCase)

	v1 := apiRoute.Group("/v1")
	{
		publisher := v1.Group("/publisher")
		{
			publisher.GET("/", publisherFinderHandler.GetAllPublishers)
			publisher.GET("/:id", publisherFinderHandler.GetPublisherById)
			publisher.POST("/", publisherCreatorHandler.CreatePublisher)
			publisher.DELETE("/:id", publisherDeleterHandler.DeletePublisher)
			publisher.PUT("/:id", publisherUpdaterHandler.UpdatePublisher)
		}
	}
}

func BookHTTPHandler(
	config config.Config,
	router *gin.Engine,
	creatorService service2.BookCreatorUseCase,
	deleterService service2.BookDeleterUseCase,
	updaterService service2.BookUpdaterUseCase,
	finderService service2.BookFinderUseCase) {
	apiRoute := SetApiRoute(router)

	bookCreatorHandler := handler2.NewCreatorBookHandler(creatorService)
	bookDeleterHandler := handler2.NewDeleterBookHandler(deleterService)
	bookUpdaterHandler := handler2.NewUpdaterBookHandler(updaterService)
	bookFinderHandler := handler2.NewFinderBookHandler(finderService)

	v1 := apiRoute.Group("/v1")
	{
		publisher := v1.Group("/books")
		{
			publisher.GET("/", bookFinderHandler.GetAllBooks)
			publisher.GET("/:id", bookFinderHandler.GetBooksById)
			publisher.Use(middleware.Auth(config)).POST("/", bookCreatorHandler.CreateBook)
			publisher.Use(middleware.Auth(config)).DELETE("/:id", bookDeleterHandler.DeleteBook)
			publisher.Use(middleware.Auth(config)).PUT("/:id", bookUpdaterHandler.UpdateBook)
		}
	}
}
