BEGIN;

CREATE TABLE IF NOT EXISTS main.authors
(
    id                    UUID         NOT NULL,
    name                  VARCHAR(128) NOT NULL,
    email                 VARCHAR(128) NOT NULL,
    password              VARCHAR(255) NOT NULL,
    bio                   VARCHAR(200) NULL,
    created_at            TIMESTAMPTZ  NOT NULL,
    updated_at            TIMESTAMPTZ  NULL,
    deleted_at            TIMESTAMPTZ  NULL,
    PRIMARY KEY (id)
);

COMMIT;