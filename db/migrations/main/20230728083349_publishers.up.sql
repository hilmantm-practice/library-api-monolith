BEGIN;

CREATE TABLE IF NOT EXISTS main.publishers
(
    id                    UUID         NOT NULL,
    name                  VARCHAR(128) NOT NULL,
    address               VARCHAR(255) NOT NULL,
    phone_number          VARCHAR(50)  NOT NULL,
    website               VARCHAR(50)  NOT NULL,
    created_at            TIMESTAMPTZ  NOT NULL,
    updated_at            TIMESTAMPTZ  NULL,
    deleted_at            TIMESTAMPTZ  NULL,
    PRIMARY KEY (id)
);

COMMIT;