package main

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"golang.org/x/net/context"
	"gorm.io/gorm"
	"library-api/config"
	"library-api/entity"
	"library-api/utils"
)

func main() {
	cfg, err := config.LoadConfig(".env")
	checkError(err)

	db, err := utils.NewPostgresGormDB(&cfg.Postgres)
	checkError(err)

	if cfg.Env == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	createSamplePublishers(db)
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

func createSamplePublishers(db *gorm.DB) {
	publishers := []entity.Publisher{
		{
			Name:        "Penguin Random House",
			Address:     "1745 Broadway, New York, NY 10019",
			PhoneNumber: "(212) 782-9000",
			Website:     "https://www.penguinrandomhouse.com/",
		},
		{
			Name:        "HarperCollins",
			Address:     "195 Broadway, New York, NY 10007",
			PhoneNumber: "(212) 207-7000",
			Website:     "https://www.harpercollins.com/",
		},
		{
			Name:        "Simon & Schuster",
			Address:     "1230 Avenue of the Americas, New York, NY 10020",
			PhoneNumber: "(212) 698-7000",
			Website:     "https://www.simonandschuster.com/",
		},
		{
			Name:        "Macmillan Publishers",
			Address:     "175 Fifth Avenue, New York, NY 10010",
			PhoneNumber: "(212) 702-2000",
			Website:     "https://www.macmillan.com/",
		},
		{
			Name:        "Hachette Book Group",
			Address:     "1290 Avenue of the Americas, New York, NY 10104",
			PhoneNumber: "(212) 641-7000",
			Website:     "https://www.hachettebookgroup.com/",
		},
		{
			Name:        "Little, Brown and Company",
			Address:     "1271 Avenue of the Americas, New York, NY 10020",
			PhoneNumber: "(212) 364-1100",
			Website:     "https://www.littlebrown.com/",
		},
		{
			Name:        "W.W. Norton & Company",
			Address:     "500 Fifth Avenue, New York, NY 10110",
			PhoneNumber: "(212) 354-5500",
			Website:     "https://www.wwnorton.com/",
		},
		{
			Name:        "The Dial Press",
			Address:     "1755 Broadway, New York, NY 10019",
			PhoneNumber: "(212) 782-9000",
			Website:     "https://www.penguinrandomhouse.com/",
		},
		{
			Name:        "Vintage Books",
			Address:     "1755 Broadway, New York, NY 10019",
			PhoneNumber: "(212) 782-9000",
			Website:     "https://www.penguinrandomhouse.com/",
		},
		{
			Name:        "Lalaland Books",
			Address:     "1756 Broadway, New York, NY 10020",
			PhoneNumber: "(212) 782-9001",
			Website:     "https://www.lalalandbooks.com/",
		},
	}

	// create 10 dummy data for publisher
	for i := 0; i < 10; i++ {
		publisherId := uuid.New()
		currentPublisher := publishers[i]

		if err := db.
			WithContext(context.Background()).
			Model(&entity.Publisher{}).
			Create(entity.NewPublisher(
				publisherId,
				currentPublisher.Name,
				currentPublisher.Address,
				currentPublisher.PhoneNumber,
				currentPublisher.Website)).Error; err != nil {
			panic(err)
		}
	}
}
