package entity

import (
	"gorm.io/gorm"
	"time"
)

// Auditable is an interface that can be embedded in structs that need to be auditable
type Auditable struct {
	CreatedAt *time.Time     `json:"created_at"`
	UpdatedAt *time.Time     `json:"updated_at" gorm:"autoUpdateTime:false"`
	DeletedAt gorm.DeletedAt `json:"deleted_at"`
}

// NewAuditable creates a new Auditable struct
func NewAuditable(createdBy string) Auditable {
	nowTime := time.Now()
	return Auditable{
		CreatedAt: &nowTime,
	}
}
