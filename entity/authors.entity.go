package entity

import (
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"time"
)

const AUTHORS_TABLE = "main.authors"

type Author struct {
	ID       uuid.UUID `json:"id"`
	Name     string    `json:"name"`
	Email    string    `json:"email"`
	Password string    `json:"password"`
	Bio      string    `json:"bio"`
	Auditable
}

// TableName specifies table name
func (model *Author) TableName() string {
	return AUTHORS_TABLE
}

func NewAuthor(id uuid.UUID, name string, email string, password string) *Author {
	passwordHash, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return &Author{
		ID:        id,
		Name:      name,
		Email:     email,
		Password:  string(passwordHash),
		Bio:       "", // TODO: fill this when update the authors, but default bio is empty
		Auditable: Auditable{},
	}
}

func (model *Author) MapUpdateFrom(from *Author) *map[string]interface{} {
	if from != nil {
		return &map[string]interface{}{
			"name":       from.Name,
			"email":      from.Email,
			"bio":        from.Bio,
			"updated_at": from.UpdatedAt,
		}
	}

	mapped := make(map[string]interface{})

	if model.Name != from.Name {
		mapped["name"] = from.Name
	}

	if model.Email != from.Email {
		mapped["email"] = from.Email
	}

	if model.Bio != from.Bio {
		mapped["bio"] = from.Bio
	}

	mapped["updated_at"] = time.Now()

	return &mapped

}
