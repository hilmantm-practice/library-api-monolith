package entity

import (
	"github.com/google/uuid"
	"time"
)

const PUBLISHERS_TABLE = "main.publishers"

type Publisher struct {
	ID          uuid.UUID `json:"id"`
	Name        string    `json:"name"`
	Address     string    `json:"address"`
	PhoneNumber string    `json:"phone_number"`
	Website     string    `json:"website"`
	Auditable
}

// TableName specifies table name
func (model *Publisher) TableName() string {
	return PUBLISHERS_TABLE
}

func NewPublisher(id uuid.UUID, name, address, phoneNumber, website string) *Publisher {
	now := time.Now()
	return &Publisher{
		ID:          id,
		Name:        name,
		Address:     address,
		PhoneNumber: phoneNumber,
		Website:     website,
		Auditable: Auditable{
			CreatedAt: &now,
		},
	}
}

func (model *Publisher) MapUpdateFrom(from *Publisher) *map[string]interface{} {
	if from != nil {
		return &map[string]interface{}{
			"name":         from.Name,
			"address":      from.Address,
			"phone_number": from.PhoneNumber,
			"website":      from.Website,
			"updated_at":   from.UpdatedAt,
		}
	}

	mapped := make(map[string]interface{})

	if model.Name != from.Name {
		mapped["name"] = from.Name
	}

	if model.Address != from.Address {
		mapped["address"] = from.Address
	}

	if model.PhoneNumber != from.PhoneNumber {
		mapped["phone_number"] = from.PhoneNumber
	}

	if model.Website != from.Website {
		mapped["website"] = from.Website
	}

	mapped["updated_at"] = time.Now()

	return &mapped

}
