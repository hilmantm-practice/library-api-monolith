package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"gorm.io/gorm"
	"library-api/config"
	"library-api/middleware"
	"library-api/modules/auth/v1/builder"
	builder2 "library-api/modules/library/v1/builder"
	"library-api/utils"
	"net/http"
	"os"
)

const (
	statusOK = "OK"
)

var environment string

// Health is a base struct for health check
type Health struct {
	Status   string `json:"status"`
	Database string `json:"database"`
	Redis    string `json:"redis"`
}

var health *Health

// splash print plain text message to console
func splash() {
	fmt.Print(`
        .__                   __                 __                
   ____ |__| ____     _______/  |______ ________/  |_  ___________ 
  / ___\|  |/    \   /  ___/\   __\__  \\_  __ \   __\/ __ \_  __ \
 / /_/  >  |   |  \  \___ \  |  |  / __ \|  | \/|  | \  ___/|  | \/
 \___  /|__|___|  / /____  > |__| (____  /__|   |__|  \___  >__|   
/_____/         \/       \/            \/                 \/       
`)
}

func main() {
	health = &Health{}
	cfg, err := config.LoadConfig(".env")
	checkError(err)

	splash()

	environment = cfg.Env

	db, err := utils.NewPostgresGormDB(&cfg.Postgres)
	checkError(err)
	health.Database = statusOK

	if cfg.Env == "production" {
		gin.SetMode(gin.ReleaseMode)
	}

	router := gin.New()

	router.Use(middleware.CORSMiddleware())
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	router.GET("/", Home)
	router.GET("/health-check", HealthGET)

	BuildHandler(*cfg, router, db)

	health.Status = statusOK
	if err := router.Run(fmt.Sprintf(":%s", cfg.Port.APP)); err != nil {
		panic(err)
	}
}

// BuildHandler is a function to build all handlers
func BuildHandler(config config.Config, router *gin.Engine, db *gorm.DB) {
	// set your handler here
	// set authentication handler
	builder.BuildAuthHandler(config, router, db)

	// set library handler
	builder2.BuildLibraryHandler(config, router, db)
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}

// OpenTracing is a function to add OpenTracing middleware
func OpenTracing() gin.HandlerFunc {
	return func(c *gin.Context) {
		wireCtx, _ := opentracing.GlobalTracer().Extract(
			opentracing.HTTPHeaders,
			opentracing.HTTPHeadersCarrier(c.Request.Header))

		serverSpan := opentracing.StartSpan(c.Request.URL.Path,
			ext.RPCServerOption(wireCtx))
		defer serverSpan.Finish()
		c.Request = c.Request.WithContext(opentracing.ContextWithSpan(c.Request.Context(), serverSpan))
		c.Next()
	}
}

// HealthGET is a function to handle health check
func HealthGET(c *gin.Context) {
	c.JSON(http.StatusOK, health)
}

// Home is a function to handle home page
func Home(c *gin.Context) {
	appVersion := os.Getenv("APP_VERSION")

	c.JSON(http.StatusOK, gin.H{
		"app_name":    "gin-starter-api",
		"environment": environment,
		"version":     appVersion,
		"status":      "running",
	})
}
