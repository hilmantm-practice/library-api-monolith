package builder

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"library-api/app"
	"library-api/config"
	"library-api/modules/auth/v1/repository"
	"library-api/modules/auth/v1/service"
)

func BuildAuthHandler(config config.Config, router *gin.Engine, db *gorm.DB) {
	// provide repository
	authRepo := repository.NewAuthRepositoryUseCase(db)

	// provide service
	authService := service.NewAuthService(config, authRepo)

	// set authentication handler
	app.AuthHTTPHandler(router, authService)
}
