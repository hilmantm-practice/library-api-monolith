package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"library-api/common/errors"
	"library-api/modules/auth/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

// AuthHandler is a handler for auth
type AuthHandler struct {
	authUseCase service.AuthUseCase
}

func NewAuthHandler(authUseCase service.AuthUseCase) *AuthHandler {
	return &AuthHandler{authUseCase: authUseCase}
}

func (handler *AuthHandler) Login(ginContext *gin.Context) {
	var loginRequest resource.LoginRequest
	if err := ginContext.ShouldBindJSON(&loginRequest); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	currentAuthor, err := handler.authUseCase.AuthValidate(ginContext, loginRequest.Email, loginRequest.Password)
	fmt.Println("error gan", err)
	if err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	token, err := handler.authUseCase.GenerateAccessToken(currentAuthor)
	if err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", resource.NewLoginResponse(token.Token)))
}
