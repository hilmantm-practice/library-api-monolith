package repository

import (
	"context"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"library-api/entity"
)

type AuthRepository struct {
	db *gorm.DB
}

type AuthRepositoryUseCase interface {
	// GetUserByEmail finds a user by email
	GetUserByEmail(ctx context.Context, email string) (*entity.Author, error)
}

func NewAuthRepositoryUseCase(db *gorm.DB) *AuthRepository {
	return &AuthRepository{db: db}
}

func (repo *AuthRepository) GetUserByEmail(ctx context.Context, email string) (*entity.Author, error) {
	result := new(entity.Author)

	if err := repo.db.WithContext(ctx).Where("email = ?", email).Find(result).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, errors.Wrap(err, "[AuthRepository-GetUserByEmail] email not found")
	}

	return result, nil
}
