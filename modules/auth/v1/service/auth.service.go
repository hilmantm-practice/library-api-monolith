package service

import (
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
	"library-api/common/errors"
	"library-api/config"
	"library-api/entity"
	"library-api/modules/auth/v1/repository"
	"library-api/utils"
)

type AuthService struct {
	config   config.Config
	authRepo repository.AuthRepositoryUseCase
}

type AuthUseCase interface {
	// AuthValidate is a function that validates the user
	AuthValidate(ctx context.Context, email, password string) (*entity.Author, error)
	// GenerateAccessToken is a function for generate token
	GenerateAccessToken(author *entity.Author) (*entity.Token, error)
}

func NewAuthService(config config.Config, authRepo repository.AuthRepositoryUseCase) *AuthService {
	return &AuthService{config: config, authRepo: authRepo}
}

func (service *AuthService) AuthValidate(ctx context.Context, email, password string) (*entity.Author, error) {
	author, err := service.authRepo.GetUserByEmail(ctx, email)
	if err != nil {
		return nil, err
	}
	if author == nil {
		return nil, errors.ErrWrongLoginCredentials.Error()
	}

	err = bcrypt.CompareHashAndPassword([]byte(author.Password), []byte(password))
	if err != nil {
		return nil, errors.ErrWrongLoginCredentials.Error()
	}

	return author, nil
}

// GenerateAccessToken is a function that generates an access token
func (service *AuthService) GenerateAccessToken(author *entity.Author) (*entity.Token, error) {
	token, err := utils.JWTEncode(service.config, author.ID, service.config.JWTConfig.Issuer)

	if err != nil {
		return nil, errors.ErrInternalServerError.Error()
	}

	return &entity.Token{
		Token: token,
	}, nil
}
