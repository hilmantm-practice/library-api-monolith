package builder

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"library-api/app"
	"library-api/config"
	"library-api/modules/library/v1/repository"
	"library-api/modules/library/v1/service"
)

func BuildLibraryHandler(config config.Config, router *gin.Engine, db *gorm.DB) {
	// repository
	authorRepo := repository.NewAuthorRepository(db)
	publisherRepo := repository.NewPublisherRepository(db)
	bookRepo := repository.NewBookRepository(db)

	// service
	creatorService := service.NewLibraryCreatorService(config, authorRepo, publisherRepo, bookRepo)
	deleterService := service.NewLibraryDeleterService(config, authorRepo, publisherRepo, bookRepo)
	updaterService := service.NewLibraryUpdaterService(config, authorRepo, publisherRepo, bookRepo)
	finderService := service.NewLibraryFinderService(config, publisherRepo, bookRepo)

	// handler
	app.AuthorHTTPHandler(config, router, creatorService, deleterService, updaterService)
	app.PublisherHTTPHandler(config, router, creatorService, deleterService, updaterService, finderService)
	app.BookHTTPHandler(config, router, creatorService, deleterService, updaterService, finderService)
}
