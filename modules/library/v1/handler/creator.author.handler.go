package handler

import (
	"github.com/gin-gonic/gin"
	"library-api/common/errors"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type AuthorCreatorHandler struct {
	authorCreator service.AuthorCreatorUseCase
}

func NewAuthorCreatorHandler(authorCreator service.AuthorCreatorUseCase) *AuthorCreatorHandler {
	return &AuthorCreatorHandler{authorCreator: authorCreator}
}

func (creator *AuthorCreatorHandler) Register(ginContext *gin.Context) {
	var registerRequest resource.RegisterRequest
	if err := ginContext.ShouldBindJSON(&registerRequest); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	createdAuthor, err := creator.authorCreator.CreateAuthor(ginContext,
		registerRequest.Name,
		registerRequest.Email,
		registerRequest.Password,
		registerRequest.Bio)
	if err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", resource.NewAuthor(createdAuthor)))
}
