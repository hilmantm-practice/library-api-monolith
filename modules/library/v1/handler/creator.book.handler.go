package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"library-api/common/errors"
	"library-api/middleware"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type CreatorBookHandler struct {
	bookCreator service.BookCreatorUseCase
}

func NewCreatorBookHandler(bookCreator service.BookCreatorUseCase) *CreatorBookHandler {
	return &CreatorBookHandler{bookCreator: bookCreator}
}

func (creator *CreatorBookHandler) CreateBook(ginContext *gin.Context) {
	var request resource.CreateBookRequest
	if err := ginContext.ShouldBindJSON(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	publisherId, err := uuid.Parse(request.PublisherId)
	if err != nil {
		ginContext.JSON(errors.ErrInvalidArgument.Code, response.ErrorAPIResponse(errors.ErrInvalidArgument.Code, errors.ErrInvalidArgument.Message))
		ginContext.Abort()
		return
	}

	fmt.Println("User Id", middleware.UserID)

	createdBook, err := creator.bookCreator.CreateBook(ginContext, publisherId, middleware.UserID, request.Title, request.ISBN)
	if err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusCreated, response.SuccessAPIResponseList(http.StatusCreated, "success", createdBook))

}
