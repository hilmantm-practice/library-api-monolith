package handler

import (
	"github.com/gin-gonic/gin"
	"library-api/common/errors"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type PublisherCreatorHandler struct {
	publisherCreator service.PublisherCreatorUseCase
}

func NewPublisherCreatorHandler(publisherCreator service.PublisherCreatorUseCase) *PublisherCreatorHandler {
	return &PublisherCreatorHandler{publisherCreator: publisherCreator}
}

func (creator *PublisherCreatorHandler) CreatePublisher(ginContext *gin.Context) {
	var request resource.NewPublisherRequest
	if err := ginContext.ShouldBindJSON(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	createdPublisher, err := creator.publisherCreator.CreatePublisher(ginContext, request.Name, request.Address, request.PhoneNumber, request.Website)
	if err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", resource.NewPublisherProfile(createdPublisher)))

}
