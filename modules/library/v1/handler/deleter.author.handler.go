package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"library-api/common/errors"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type AuthorDeleterHandler struct {
	authorDeleter service.AuthorDeleterUseCase
}

func NewAuthorDeleterHandler(authorDeleter service.AuthorDeleterUseCase) *AuthorDeleterHandler {
	return &AuthorDeleterHandler{authorDeleter: authorDeleter}
}

func (creator *AuthorDeleterHandler) DeleteAuthor(ginContext *gin.Context) {
	var request resource.AuthorDetailUri
	if err := ginContext.ShouldBindUri(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	reqID, err := uuid.Parse(request.ID)
	if err != nil {
		ginContext.JSON(errors.ErrInvalidArgument.Code, response.ErrorAPIResponse(errors.ErrInvalidArgument.Code, errors.ErrInvalidArgument.Message))
		ginContext.Abort()
		return
	}

	if err := creator.authorDeleter.DeleteAuthor(ginContext, reqID); err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", nil))
}
