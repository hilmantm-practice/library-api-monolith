package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"library-api/common/errors"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type DeleterBookHandler struct {
	bookDeleter service.BookDeleterUseCase
}

func NewDeleterBookHandler(bookDeleter service.BookDeleterUseCase) *DeleterBookHandler {
	return &DeleterBookHandler{bookDeleter: bookDeleter}
}

func (handler *DeleterBookHandler) DeleteBook(ginContext *gin.Context) {
	var request resource.BookDetailUri
	if err := ginContext.ShouldBindUri(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	reqID, err := uuid.Parse(request.ID)
	if err != nil {
		ginContext.JSON(errors.ErrInvalidArgument.Code, response.ErrorAPIResponse(errors.ErrInvalidArgument.Code, errors.ErrInvalidArgument.Message))
		ginContext.Abort()
		return
	}

	if err := handler.bookDeleter.DeleteBook(ginContext, reqID); err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", nil))
}
