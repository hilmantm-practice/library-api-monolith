package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"library-api/common/errors"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type PublisherDeleterHandler struct {
	publisherDeleter service.PublisherDeleterUseCase
}

func NewPublisherDeleterHandler(publisherDeleter service.PublisherDeleterUseCase) *PublisherDeleterHandler {
	return &PublisherDeleterHandler{publisherDeleter: publisherDeleter}
}

func (handler *PublisherDeleterHandler) DeletePublisher(ginContext *gin.Context) {
	var request resource.PublisherDetailUri
	if err := ginContext.ShouldBindUri(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	reqID, err := uuid.Parse(request.ID)
	if err != nil {
		ginContext.JSON(errors.ErrInvalidArgument.Code, response.ErrorAPIResponse(errors.ErrInvalidArgument.Code, errors.ErrInvalidArgument.Message))
		ginContext.Abort()
		return
	}

	if err := handler.publisherDeleter.DeletePublisher(ginContext, reqID); err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", nil))
}
