package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"library-api/common/errors"
	"library-api/middleware"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type FinderBookHandler struct {
	bookFinder service.BookFinderUseCase
}

func NewFinderBookHandler(bookFinder service.BookFinderUseCase) *FinderBookHandler {
	return &FinderBookHandler{bookFinder: bookFinder}
}

func (handler *FinderBookHandler) GetAllBooks(ginContext *gin.Context) {
	if middleware.UserID.String() == "00000000-0000-0000-0000-000000000000" {
		res, err := handler.bookFinder.GetAllBooks(ginContext)
		if err != nil {
			parseError := errors.ParseError(err)
			ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
			ginContext.Abort()
			return
		}
		ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", resource.NewBookProfileFromList(res)))
	} else {
		res, err := handler.bookFinder.GetBooksByAuthor(ginContext, middleware.UserID)
		if err != nil {
			parseError := errors.ParseError(err)
			ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
			ginContext.Abort()
			return
		}
		ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", resource.NewBookProfileFromList(res)))
	}

}

func (handler *FinderBookHandler) GetBooksById(ginContext *gin.Context) {
	var request resource.BookDetailUri
	if err := ginContext.ShouldBindUri(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	reqID, err := uuid.Parse(request.ID)
	if err != nil {
		ginContext.JSON(errors.ErrInvalidArgument.Code, response.ErrorAPIResponse(errors.ErrInvalidArgument.Code, errors.ErrInvalidArgument.Message))
		ginContext.Abort()
		return
	}

	res, err := handler.bookFinder.GetBookById(ginContext, reqID)
	if err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	if res == nil {
		ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusNotFound, "success", "Not Found"))
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", resource.NewBookProfile(res)))

}
