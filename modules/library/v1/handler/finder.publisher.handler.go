package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"library-api/common/errors"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type PublisherFinderHandler struct {
	publisherFinder service.PublisherFinderUseCase
}

func NewPublisherFinderHandler(publisherFinder service.PublisherFinderUseCase) *PublisherFinderHandler {
	return &PublisherFinderHandler{publisherFinder: publisherFinder}
}

func (handler *PublisherFinderHandler) GetAllPublishers(ginContext *gin.Context) {
	res, err := handler.publisherFinder.GetAllPublisher(ginContext)

	if err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", res))
}

func (handler *PublisherFinderHandler) GetPublisherById(ginContext *gin.Context) {
	var request resource.PublisherDetailUri
	if err := ginContext.ShouldBindUri(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	reqID, err := uuid.Parse(request.ID)
	if err != nil {
		ginContext.JSON(errors.ErrInvalidArgument.Code, response.ErrorAPIResponse(errors.ErrInvalidArgument.Code, errors.ErrInvalidArgument.Message))
		ginContext.Abort()
		return
	}

	res, err := handler.publisherFinder.GetPublisherById(ginContext, reqID)
	if err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	if res == nil {
		ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusNotFound, "success", "Not Found"))
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", resource.NewPublisherProfile(res)))

}
