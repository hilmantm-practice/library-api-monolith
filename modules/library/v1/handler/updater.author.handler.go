package handler

import (
	"github.com/gin-gonic/gin"
	"library-api/common/errors"
	"library-api/entity"
	"library-api/middleware"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type AuthorUpdaterHandler struct {
	authorUpdater service.AuthorUpdaterUseCase
}

func NewAuthorUpdaterHandler(authorUpdater service.AuthorUpdaterUseCase) *AuthorUpdaterHandler {
	return &AuthorUpdaterHandler{authorUpdater: authorUpdater}
}

func (updater *AuthorUpdaterHandler) UpdateAuthor(ginContext *gin.Context) {
	var request resource.AuthorDetailUri
	if err := ginContext.ShouldBindUri(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	var bodyRequest resource.AuthorUpdateRequest
	if err := ginContext.ShouldBindJSON(&bodyRequest); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	authorEntity := entity.Author{
		ID:    middleware.UserID,
		Name:  bodyRequest.Name,
		Email: bodyRequest.Email,
		Bio:   bodyRequest.Bio,
	}
	if err := updater.authorUpdater.UpdateAuthor(ginContext, &authorEntity); err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", resource.NewAuthor(&authorEntity)))
}
