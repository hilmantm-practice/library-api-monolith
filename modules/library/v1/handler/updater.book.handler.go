package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"library-api/common/errors"
	"library-api/entity"
	"library-api/middleware"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type UpdaterBookHandler struct {
	bookUpdater service.BookUpdaterUseCase
}

func NewUpdaterBookHandler(bookUpdater service.BookUpdaterUseCase) *UpdaterBookHandler {
	return &UpdaterBookHandler{bookUpdater: bookUpdater}
}

func (handler *UpdaterBookHandler) UpdateBook(ginContext *gin.Context) {
	var request resource.BookDetailUri
	if err := ginContext.ShouldBindUri(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	reqID, err := uuid.Parse(request.ID)
	if err != nil {
		ginContext.JSON(errors.ErrInvalidArgument.Code, response.ErrorAPIResponse(errors.ErrInvalidArgument.Code, errors.ErrInvalidArgument.Message))
		ginContext.Abort()
		return
	}

	var bodyRequest resource.UpdateBookRequest
	if err := ginContext.ShouldBindJSON(&bodyRequest); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	publisherId, err := uuid.Parse(bodyRequest.PublisherId)
	if err != nil {
		ginContext.JSON(errors.ErrInvalidArgument.Code, response.ErrorAPIResponse(errors.ErrInvalidArgument.Code, errors.ErrInvalidArgument.Message))
		ginContext.Abort()
		return
	}

	bookEntity := entity.Book{
		ID:          reqID,
		PublisherId: publisherId,
		UserId:      middleware.UserID,
		Title:       bodyRequest.Name,
		ISBN:        bodyRequest.ISBN,
	}
	if err := handler.bookUpdater.UpdateBook(ginContext, &bookEntity); err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", bookEntity))
}
