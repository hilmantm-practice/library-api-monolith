package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"library-api/common/errors"
	"library-api/entity"
	"library-api/modules/library/v1/service"
	"library-api/resource"
	"library-api/response"
	"net/http"
)

type PublisherUpdaterHandler struct {
	publisherUpdater service.PublisherUpdaterUseCase
}

func NewPublisherUpdaterHandler(publisherUpdater service.PublisherUpdaterUseCase) *PublisherUpdaterHandler {
	return &PublisherUpdaterHandler{publisherUpdater: publisherUpdater}
}

func (handler *PublisherUpdaterHandler) UpdatePublisher(ginContext *gin.Context) {
	var request resource.PublisherDetailUri
	if err := ginContext.ShouldBindUri(&request); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	reqID, err := uuid.Parse(request.ID)
	if err != nil {
		ginContext.JSON(errors.ErrInvalidArgument.Code, response.ErrorAPIResponse(errors.ErrInvalidArgument.Code, errors.ErrInvalidArgument.Message))
		ginContext.Abort()
		return
	}

	var bodyRequest resource.PublisherUpdateRequest
	if err := ginContext.ShouldBindJSON(&bodyRequest); err != nil {
		ginContext.JSON(http.StatusBadRequest, response.ErrorAPIResponse(http.StatusBadRequest, err.Error()))
		ginContext.Abort()
		return
	}

	publisherEntity := entity.Publisher{
		ID:          reqID,
		Name:        bodyRequest.Name,
		Address:     bodyRequest.Address,
		PhoneNumber: bodyRequest.PhoneNumber,
		Website:     bodyRequest.Website,
	}
	if err := handler.publisherUpdater.UpdatePublisher(ginContext, &publisherEntity); err != nil {
		parseError := errors.ParseError(err)
		ginContext.JSON(parseError.Code, response.ErrorAPIResponse(parseError.Code, parseError.Message))
		ginContext.Abort()
		return
	}

	ginContext.JSON(http.StatusOK, response.SuccessAPIResponseList(http.StatusOK, "success", resource.NewPublisherProfile(&publisherEntity)))
}
