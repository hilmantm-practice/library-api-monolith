package repository

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"library-api/entity"
	"log"
	"time"
)

type AuthorRepository struct {
	db *gorm.DB
}

type AuthorRepositoryUseCase interface {
	// GetUserByEmail is a function to get user by email
	GetUserByEmail(ctx context.Context, email string) (*entity.Author, error)
	// GetUserById is a function to get user by id
	GetUserById(ctx context.Context, authorId string) (*entity.Author, error)
	// CreateAuthor is a function to create library
	CreateAuthor(ctx context.Context, user *entity.Author) error
	// DeleteAuthor is a function to delete author with soft delete method
	DeleteAuthor(ctx context.Context, authorId uuid.UUID) error
	// UpdateAuthor is a function to update atuhor
	UpdateAuthor(ctx context.Context, newAuthor *entity.Author) error
}

func NewAuthorRepository(db *gorm.DB) *AuthorRepository {
	return &AuthorRepository{db: db}
}

func (repo *AuthorRepository) GetUserByEmail(ctx context.Context, email string) (*entity.Author, error) {
	result := new(entity.Author)

	if err := repo.db.WithContext(ctx).Where("email = ?", email).Find(result).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, errors.Wrap(err, "[AuthorRepository-GetUserByEmail] email not found")
	}

	return result, nil
}

func (repo *AuthorRepository) GetUserById(ctx context.Context, authorId string) (*entity.Author, error) {
	result := new(entity.Author)

	if err := repo.db.WithContext(ctx).Where("id = ?", authorId).First(result).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, errors.Wrap(err, "[AuthorRepository-GetUserById] user id not found")
	}

	return result, nil
}

func (repo *AuthorRepository) CreateAuthor(ctx context.Context, author *entity.Author) error {
	if err := repo.db.WithContext(ctx).Model(&entity.Author{}).Create(author).Error; err != nil {
		return errors.Wrap(err, "[AuthorRepository-CreateUser] error while creating user")
	}
	return nil
}

func (repo *AuthorRepository) DeleteAuthor(ctx context.Context, authorId uuid.UUID) error {
	if err := repo.db.
		WithContext(ctx).
		Model(&entity.Author{}).
		Where(`id = ?`, authorId).
		Delete(&entity.Author{}, "id = ?", authorId).Error; err != nil {
		return errors.Wrap(err, "[UserRepository-DeleteAdmin] error when updating user data")
	}
	return nil
}

func (repo *AuthorRepository) UpdateAuthor(ctx context.Context, newAuthor *entity.Author) error {
	nowTime := time.Now()
	newAuthor.UpdatedAt = &nowTime

	if err := repo.db.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		sourceEntity := new(entity.Author)
		if err := tx.Clauses(clause.Locking{Strength: "UPDATE"}).Find(&sourceEntity, newAuthor.ID).Error; err != nil {
			return err
		}
		if err := tx.Model(&entity.Author{}).Where(`id = ?`, newAuthor.ID).UpdateColumns(sourceEntity.MapUpdateFrom(newAuthor)).Error; err != nil {
			log.Println("[UserRepository-UpdateUser]", err)
			return err
		}
		return nil
	}); err != nil {
		return err
	}

	return nil
}
