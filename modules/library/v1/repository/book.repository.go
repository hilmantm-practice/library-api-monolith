package repository

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"library-api/entity"
	"log"
	"time"
)

type BookRepository struct {
	db *gorm.DB
}

type BookRepositoryUseCase interface {
	// GetBooksByAuthor is a function to get all authors books
	GetBooksByAuthor(ctx context.Context, authorId uuid.UUID) ([]*entity.Book, error)
	// GetAllBooks is a function to get all books (without authentication)
	GetAllBooks(ctx context.Context) ([]*entity.Book, error)
	// GetBookById is a function to get book by id
	GetBookById(ctx context.Context, bookId uuid.UUID) (*entity.Book, error)
	// CreateBook is a function to create book
	CreateBook(ctx context.Context, book *entity.Book) error
	// DeleteBook is a function to delete book with soft delete method
	DeleteBook(ctx context.Context, bookId uuid.UUID) error
	// UpdateBook is a function to update book
	UpdateBook(ctx context.Context, newBook *entity.Book) error
}

func NewBookRepository(db *gorm.DB) *BookRepository {
	return &BookRepository{db: db}
}

func (repo *BookRepository) GetBooksByAuthor(ctx context.Context, authorId uuid.UUID) ([]*entity.Book, error) {
	results := make([]*entity.Book, 0)

	if err := repo.db.WithContext(ctx).Model(&entity.Book{}).Where("user_id = ?", authorId).Preload("Publisher").Preload("Author").Find(&results).Error; err != nil {
		return nil, errors.Wrap(err, "[BookRepository-GetBooksByAuthor] error while get all books")
	}

	return results, nil
}

func (repo *BookRepository) GetAllBooks(ctx context.Context) ([]*entity.Book, error) {
	results := make([]*entity.Book, 0)

	if err := repo.db.WithContext(ctx).Model(&entity.Book{}).Preload("Publisher").Preload("Author").Find(&results).Error; err != nil {
		return nil, errors.Wrap(err, "[BookRepository-GetAllBooks] error while get all books")
	}

	return results, nil
}

func (repo *BookRepository) GetBookById(ctx context.Context, bookId uuid.UUID) (*entity.Book, error) {
	result := new(entity.Book)

	find := repo.db.WithContext(ctx).Model(entity.Book{}).Where("id = ?", bookId).Preload("Publisher").Preload("Author").First(result).Debug()
	if errors.Is(find.Error, gorm.ErrRecordNotFound) {
		return nil, nil
	}
	if err := find.Error; err != nil {
		return nil, errors.Wrap(err, "[BookRepository-GetBookById] book not found")
	}
	return result, nil
}

func (repo *BookRepository) CreateBook(ctx context.Context, book *entity.Book) error {
	if err := repo.db.WithContext(ctx).Model(&entity.Book{}).Create(book).Error; err != nil {
		return errors.Wrap(err, "[BookRepository-CreateBook] error while creating book")
	}
	return nil
}

func (repo *BookRepository) DeleteBook(ctx context.Context, bookId uuid.UUID) error {
	if err := repo.db.WithContext(ctx).Model(&entity.Book{}).Where("id = ?", bookId).Delete(&entity.Book{}, "id = ?", bookId).Error; err != nil {
		return errors.Wrap(err, "[BookRepository-DeleteBook] error when delete book data")
	}

	return nil
}

func (repo *BookRepository) UpdateBook(ctx context.Context, newBook *entity.Book) error {
	nowTime := time.Now()
	newBook.UpdatedAt = &nowTime

	if err := repo.db.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		sourceEntity := new(entity.Book)
		if err := tx.Clauses(clause.Locking{Strength: "UPDATE"}).Find(&sourceEntity, newBook.ID).Error; err != nil {
			return err
		}
		if err := tx.Model(&entity.Book{}).Where(`id = ?`, newBook.ID).UpdateColumns(sourceEntity.MapUpdateFrom(newBook)).Error; err != nil {
			log.Println("[BookRepository-UpdateBook]", err)
			return err
		}
		return nil
	}); err != nil {
		return err
	}

	return nil
}
