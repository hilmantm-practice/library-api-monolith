package repository

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"library-api/entity"
	"log"
	"time"
)

type PublisherRepository struct {
	db *gorm.DB
}

type PublisherRepositoryUseCase interface {
	// CreatePublisher is a function to create publisher
	CreatePublisher(ctx context.Context, publiser *entity.Publisher) error
	// DeletePublisher is a function to delete publisher with soft delete
	DeletePublisher(ctx context.Context, publisherId uuid.UUID) error
	// UpdatePublisher is a function to update publisher
	UpdatePublisher(ctx context.Context, newPublisher *entity.Publisher) error
	// GetAllPublisher is a function to get all publisher
	GetAllPublisher(ctx context.Context) ([]*entity.Publisher, error)
	// GetPublisherById is a function to get single publisher based on publisher id
	GetPublisherById(ctx context.Context, publisherId uuid.UUID) (*entity.Publisher, error)
}

func NewPublisherRepository(db *gorm.DB) *PublisherRepository {
	return &PublisherRepository{db: db}
}

func (repo *PublisherRepository) GetAllPublisher(ctx context.Context) ([]*entity.Publisher, error) {
	results := make([]*entity.Publisher, 0)

	if err := repo.db.WithContext(ctx).Model(&entity.Publisher{}).Find(&results).Error; err != nil {
		return nil, errors.Wrap(err, "[PublisherRepository-GetAllPublisher] error while get all publishers")
	}

	return results, nil
}

func (repo *PublisherRepository) GetPublisherById(ctx context.Context, publisherId uuid.UUID) (*entity.Publisher, error) {
	result := new(entity.Publisher)

	find := repo.db.WithContext(ctx).Model(entity.Publisher{}).Where("id = ?", publisherId).First(&result)
	if errors.Is(find.Error, gorm.ErrRecordNotFound) {
		return nil, nil
	}
	if err := find.Error; err != nil {
		return nil, errors.Wrap(err, "[PublisherRepository-GetPublisherById] error while find publisher")
	}

	return result, nil
}

func (repo *PublisherRepository) CreatePublisher(ctx context.Context, publiser *entity.Publisher) error {
	if err := repo.db.WithContext(ctx).Model(&entity.Publisher{}).Create(publiser).Error; err != nil {
		return errors.Wrap(err, "[PublisherRepository-CreatePublisher] error while creating publisher")
	}

	return nil
}

func (repo *PublisherRepository) DeletePublisher(ctx context.Context, publisherId uuid.UUID) error {
	if err := repo.db.WithContext(ctx).Model(&entity.Publisher{}).Where("id = ?", publisherId).Delete(&entity.Publisher{}, "id = ?", publisherId).Error; err != nil {
		return errors.Wrap(err, "[PublisherRepository-DeletePublisher] error when delete publisher data")
	}

	return nil
}

func (repo *PublisherRepository) UpdatePublisher(ctx context.Context, newPublisher *entity.Publisher) error {
	nowTime := time.Now()
	newPublisher.UpdatedAt = &nowTime

	if err := repo.db.WithContext(ctx).Transaction(func(tx *gorm.DB) error {
		sourceEntity := new(entity.Publisher)
		if err := tx.Clauses(clause.Locking{Strength: "UPDATE"}).Find(&sourceEntity, newPublisher.ID).Error; err != nil {
			return err
		}
		if err := tx.Model(&entity.Publisher{}).Where(`id = ?`, newPublisher.ID).UpdateColumns(sourceEntity.MapUpdateFrom(newPublisher)).Error; err != nil {
			log.Println("[PublisherRepository-UpdatePublisher]", err)
			return err
		}
		return nil
	}); err != nil {
		return err
	}

	return nil
}
