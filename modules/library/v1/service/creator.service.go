package service

import (
	"github.com/google/uuid"
	"golang.org/x/net/context"
	"library-api/common/errors"
	"library-api/config"
	"library-api/entity"
	repository2 "library-api/modules/library/v1/repository"
)

type LibraryCreatorService struct {
	config        config.Config
	authorRepo    repository2.AuthorRepositoryUseCase
	publisherRepo repository2.PublisherRepositoryUseCase
	bookRepo      repository2.BookRepositoryUseCase
}

// TODO: Make another usecase for another table
type AuthorCreatorUseCase interface {
	CreateAuthor(ctx context.Context, name, email, password, bio string) (*entity.Author, error)
}

type PublisherCreatorUseCase interface {
	CreatePublisher(ctx context.Context, name, address, phoneNumber, website string) (*entity.Publisher, error)
}

type BookCreatorUseCase interface {
	CreateBook(ctx context.Context, publisherId, authorId uuid.UUID, title, isbn string) (*entity.Book, error)
}

func NewLibraryCreatorService(
	config config.Config,
	authorRepo repository2.AuthorRepositoryUseCase,
	publisherRepo repository2.PublisherRepositoryUseCase,
	bookRepo repository2.BookRepositoryUseCase) *LibraryCreatorService {
	return &LibraryCreatorService{
		config:        config,
		authorRepo:    authorRepo,
		publisherRepo: publisherRepo,
		bookRepo:      bookRepo,
	}
}

func (service *LibraryCreatorService) CreateAuthor(ctx context.Context, name, email, password, bio string) (*entity.Author, error) {
	// find author, if exist don't create author
	existAuthor, err := service.authorRepo.GetUserByEmail(ctx, email)
	if err != nil {
		return nil, err
	}
	if existAuthor != nil {
		return nil, errors.ErrUserExist.Error()
	}

	// if author doesn't exist, create author
	newAuthor := entity.NewAuthor(uuid.New(), name, email, password)
	if err = service.authorRepo.CreateAuthor(ctx, newAuthor); err != nil {
		return nil, err
	}
	return newAuthor, nil
}

func (service *LibraryCreatorService) CreatePublisher(ctx context.Context, name, address, phoneNumber, website string) (*entity.Publisher, error) {
	newPublisher := entity.NewPublisher(uuid.New(), name, address, phoneNumber, website)
	if err := service.publisherRepo.CreatePublisher(ctx, newPublisher); err != nil {
		return nil, err
	}
	return newPublisher, nil
}

func (service *LibraryCreatorService) CreateBook(ctx context.Context, publisherId, authorId uuid.UUID, title, isbn string) (*entity.Book, error) {
	newBook := entity.NewBook(uuid.New(), publisherId, authorId, title, isbn)
	if err := service.bookRepo.CreateBook(ctx, newBook); err != nil {
		return nil, err
	}
	return newBook, nil
}
