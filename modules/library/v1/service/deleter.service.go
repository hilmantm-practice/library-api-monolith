package service

import (
	"github.com/google/uuid"
	"golang.org/x/net/context"
	"library-api/common/errors"
	"library-api/config"
	"library-api/modules/library/v1/repository"
)

type LibraryDeleterService struct {
	config        config.Config
	authorRepo    repository.AuthorRepositoryUseCase
	publisherRepo repository.PublisherRepositoryUseCase
	bookRepo      repository.BookRepositoryUseCase
}

// TODO: Make another usecase for another table
type AuthorDeleterUseCase interface {
	// DeleteAuthor is a function to delete author with soft delete method
	DeleteAuthor(ctx context.Context, authorId uuid.UUID) error
}

type PublisherDeleterUseCase interface {
	// DeletePublisher is a function to delete publisher with soft delete method
	DeletePublisher(ctx context.Context, publisherId uuid.UUID) error
}

type BookDeleterUseCase interface {
	DeleteBook(ctx context.Context, bookId uuid.UUID) error
}

func NewLibraryDeleterService(
	config config.Config,
	authorRepo repository.AuthorRepositoryUseCase,
	publisherRepo repository.PublisherRepositoryUseCase,
	bookRepo repository.BookRepositoryUseCase) *LibraryDeleterService {
	return &LibraryDeleterService{
		config:        config,
		authorRepo:    authorRepo,
		publisherRepo: publisherRepo,
		bookRepo:      bookRepo,
	}
}

func (service *LibraryDeleterService) DeleteAuthor(ctx context.Context, authorId uuid.UUID) error {
	if err := service.authorRepo.DeleteAuthor(ctx, authorId); err != nil {
		return errors.ErrInternalServerError.Error()
	}
	return nil
}

func (service *LibraryDeleterService) DeletePublisher(ctx context.Context, publisherId uuid.UUID) error {
	if err := service.publisherRepo.DeletePublisher(ctx, publisherId); err != nil {
		return err
	}
	return nil
}

func (service *LibraryDeleterService) DeleteBook(ctx context.Context, bookId uuid.UUID) error {
	if err := service.bookRepo.DeleteBook(ctx, bookId); err != nil {
		return err
	}
	return nil
}
