package service

import (
	"github.com/google/uuid"
	"golang.org/x/net/context"
	"library-api/config"
	"library-api/entity"
	"library-api/modules/library/v1/repository"
)

type LibraryFinderService struct {
	config        config.Config
	publisherRepo repository.PublisherRepositoryUseCase
	bookRepo      repository.BookRepositoryUseCase
}

type PublisherFinderUseCase interface {
	// GetAllPublisher is a function to get all publisher
	GetAllPublisher(ctx context.Context) ([]*entity.Publisher, error)
	// GetPublisherById is a function to get single publisher based on publisher id
	GetPublisherById(ctx context.Context, publisherId uuid.UUID) (*entity.Publisher, error)
}

type BookFinderUseCase interface {
	// GetBooksByAuthor is a function to get all authors books
	GetBooksByAuthor(ctx context.Context, authorId uuid.UUID) ([]*entity.Book, error)
	// GetAllBooks is a function to get all books (without authentication)
	GetAllBooks(ctx context.Context) ([]*entity.Book, error)
	// GetBookById is a function to get book by id
	GetBookById(ctx context.Context, bookId uuid.UUID) (*entity.Book, error)
}

func NewLibraryFinderService(
	config config.Config,
	publisherRepo repository.PublisherRepositoryUseCase,
	bookRepo repository.BookRepositoryUseCase) *LibraryFinderService {
	return &LibraryFinderService{config: config, publisherRepo: publisherRepo, bookRepo: bookRepo}
}

func (service *LibraryFinderService) GetAllPublisher(ctx context.Context) ([]*entity.Publisher, error) {
	result, err := service.publisherRepo.GetAllPublisher(ctx)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (service *LibraryFinderService) GetPublisherById(ctx context.Context, publisherId uuid.UUID) (*entity.Publisher, error) {
	result, err := service.publisherRepo.GetPublisherById(ctx, publisherId)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (service *LibraryFinderService) GetBooksByAuthor(ctx context.Context, authorId uuid.UUID) ([]*entity.Book, error) {
	result, err := service.bookRepo.GetBooksByAuthor(ctx, authorId)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (service *LibraryFinderService) GetAllBooks(ctx context.Context) ([]*entity.Book, error) {
	result, err := service.bookRepo.GetAllBooks(ctx)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (service *LibraryFinderService) GetBookById(ctx context.Context, bookId uuid.UUID) (*entity.Book, error) {
	result, err := service.bookRepo.GetBookById(ctx, bookId)
	if err != nil {
		return nil, err
	}
	return result, nil
}
