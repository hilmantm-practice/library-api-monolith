package service

import (
	"golang.org/x/net/context"
	"library-api/common/errors"
	"library-api/config"
	"library-api/entity"
	"library-api/modules/library/v1/repository"
)

type LibraryUpdaterService struct {
	config        config.Config
	authorRepo    repository.AuthorRepositoryUseCase
	publisherRepo repository.PublisherRepositoryUseCase
	bookRepo      repository.BookRepositoryUseCase
}

type AuthorUpdaterUseCase interface {
	UpdateAuthor(ctx context.Context, author *entity.Author) error
}

type PublisherUpdaterUseCase interface {
	UpdatePublisher(ctx context.Context, publisher *entity.Publisher) error
}

type BookUpdaterUseCase interface {
	UpdateBook(ctx context.Context, book *entity.Book) error
}

func NewLibraryUpdaterService(
	config config.Config,
	authorRepo repository.AuthorRepositoryUseCase,
	publisherRepo repository.PublisherRepositoryUseCase,
	bookRepo repository.BookRepositoryUseCase) *LibraryUpdaterService {
	return &LibraryUpdaterService{config: config, authorRepo: authorRepo, publisherRepo: publisherRepo, bookRepo: bookRepo}
}

func (service *LibraryUpdaterService) UpdateAuthor(ctx context.Context, author *entity.Author) error {
	// prevent author with filled deleted_at can't update
	existUser, err := service.authorRepo.GetUserById(ctx, author.ID.String())
	if err != nil {
		return errors.ErrInternalServerError.Error()
	}
	if existUser == nil {
		return errors.ErrRecordNotFound.Error()
	}

	if err := service.authorRepo.UpdateAuthor(ctx, author); err != nil {
		return errors.ErrInternalServerError.Error()
	}

	return nil
}

func (service *LibraryUpdaterService) UpdatePublisher(ctx context.Context, publisher *entity.Publisher) error {
	if err := service.publisherRepo.UpdatePublisher(ctx, publisher); err != nil {
		return err
	}

	return nil
}

func (service *LibraryUpdaterService) UpdateBook(ctx context.Context, book *entity.Book) error {
	if err := service.bookRepo.UpdateBook(ctx, book); err != nil {
		return err
	}

	return nil
}
