package resource

import (
	"library-api/entity"
	"time"
)

type AuthorProfile struct {
	ID        string     `json:"id"`
	Name      string     `json:"name"`
	Email     string     `json:"email"`
	Bio       string     `json:"bio"`
	CreatedAt *time.Time `json:"created_at,omitempty"`
	UpdatedAt *time.Time `json:"updated_at,omitempty"`
}

func NewAuthor(author *entity.Author) *AuthorProfile {
	return &AuthorProfile{
		ID:        author.ID.String(),
		Name:      author.Name,
		Email:     author.Email,
		Bio:       author.Bio,
		CreatedAt: author.CreatedAt,
		UpdatedAt: author.UpdatedAt,
	}
}

type AuthorDetailUri struct {
	ID string `uri:"id" binding:"required"`
}

type AuthorUpdateRequest struct {
	Name  string `form:"name,omitempty" json:"name,omitempty" binding:"required"`
	Email string `form:"email,omitempty" json:"email,omitempty" binding:"required"`
	Bio   string `form:"bio,omitempty" json:"bio,omitempty"`
}

type RegisterRequest struct {
	Name     string `form:"name,omitempty" json:"name,omitempty" binding:"required"`
	Email    string `form:"email,omitempty" json:"email,omitempty" binding:"required"`
	Password string `form:"password,omitempty" json:"password,omitempty" binding:"required"`
	Bio      string `form:"bio,omitempty" json:"bio,omitempty"`
}
