package resource

import (
	"library-api/entity"
	"time"
)

type BookProfile struct {
	ID          string            `json:"id"`
	Publisher   *PublisherProfile `json:"publisher"`
	Author      *AuthorProfile    `json:"author"`
	Book        string            `json:"book"`
	ISBN        string            `json:"ISBN"`
	PublisherId string            `json:"publisher_id,omitempty"`
	AuthorId    string            `json:"author_id,omitempty"`
	CreatedAt   *time.Time        `json:"created_at,omitempty"`
	UpdatedAt   *time.Time        `json:"updated_at,omitempty"`
}

func NewBookProfile(book *entity.Book) *BookProfile {
	return &BookProfile{
		ID:        book.ID.String(),
		Publisher: NewPublisherProfile(book.Publisher),
		Author:    NewAuthor(book.Author),
		Book:      book.Title,
		ISBN:      book.ISBN,
		CreatedAt: book.CreatedAt,
		UpdatedAt: book.UpdatedAt,
	}
}

func NewBookProfileFromList(books []*entity.Book) []*BookProfile {
	result := make([]*BookProfile, 0)
	for _, book := range books {
		result = append(result, NewBookProfile(book))
	}
	return result
}

type BookDetailUri struct {
	ID string `uri:"id" binding:"required"`
}

type UpdateBookRequest struct {
	PublisherId string `form:"publisher_id,omitempty" json:"publisher_id,omitempty" binding:"required"`
	Name        string `form:"title,omitempty" json:"title,omitempty" binding:"required"`
	ISBN        string `form:"isbn,omitempty" json:"isbn,omitempty" binding:"required"`
}

type CreateBookRequest struct {
	PublisherId string `form:"publisher_id,omitempty" json:"publisher_id,omitempty" binding:"required"`
	Title       string `form:"title,omitempty" json:"title,omitempty" binding:"required"`
	ISBN        string `form:"isbn,omitempty" json:"isbn,omitempty" binding:"required"`
}
