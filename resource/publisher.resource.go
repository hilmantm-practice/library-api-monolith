package resource

import (
	"library-api/entity"
	"time"
)

type PublisherProfile struct {
	ID          string     `json:"id"`
	Name        string     `json:"name"`
	Address     string     `json:"address"`
	PhoneNumber string     `json:"phone_number"`
	Website     string     `json:"website"`
	CreatedAt   *time.Time `json:"created_at,omitempty"`
	UpdatedAt   *time.Time `json:"updated_at,omitempty"`
}

func NewPublisherProfile(publisher *entity.Publisher) *PublisherProfile {
	return &PublisherProfile{
		ID:          publisher.ID.String(),
		Name:        publisher.Name,
		Address:     publisher.Address,
		PhoneNumber: publisher.PhoneNumber,
		Website:     publisher.Website,
		CreatedAt:   publisher.CreatedAt,
		UpdatedAt:   publisher.UpdatedAt,
	}
}

type NewPublisherRequest struct {
	Name        string `json:"name" binding:"required"`
	Address     string `json:"address" binding:"required"`
	PhoneNumber string `json:"phone_number" binding:"required"`
	Website     string `json:"website" binding:"required"`
}

type PublisherUpdateRequest struct {
	Name        string `json:"name" binding:"required"`
	Address     string `json:"address" binding:"required"`
	PhoneNumber string `json:"phone_number" binding:"required"`
	Website     string `json:"website" binding:"required"`
}

type PublisherDetailUri struct {
	ID string `uri:"id" binding:"required"`
}
